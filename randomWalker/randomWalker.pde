void setup(){
  // size( width, height, 
  size(1080,720,P3D);  // 1920 , 1080
  camera(0,0, -(height/2) / tan(PI*30/180),  //camera position 
        0,0,0,   // eye position 
        0,-1,0);  // up vector
 
}

Walker walker = new Walker();

void draw(){
  walker.render();
  walker.randomWalker();
  
  //float randomValue = floor(random(4));
  //println(randomValue);
}
