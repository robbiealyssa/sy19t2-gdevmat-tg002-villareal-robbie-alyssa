void setup(){
  // size( width, height, 
  size(1080,720,P3D);  // 1920 , 1080
  camera(0,0, -(height/2) / tan(PI*30/180),  //camera position 
        0,0,0,   // eye position 
        0,-1,0);  // up vector
        
   widthOfWave = width + 20;
   dx = (TWO_PI / period) * spacing;
   yValue = new float [widthOfWave/spacing];
}

void draw(){   // gets called evry frame
  
  background(130);
  drawCartesianPlane();
  drawLinearFunction();
  drawQuadraticFunction();
  drawCircleFunction();
  radius++;
  drawWaveFunction();
  rederStroke();
  
}

void drawCartesianPlane(){
  
 line(300,0,-300,0);
 line(0,300,0,-300);
 
 for(int i=-300; i<=300;i+=10){
   line(i,-5,i,5);
   line(-5,i,5,i);
 }
}

void drawLinearFunction(){
  
 /*
 f(x) = x+2;
 y = x+2;
 let x be 4 , then y=6 (4,6);
 let x be -5, then y=-3 (-5,-3);
 */
 
  for(int x=-200; x<= 200;x++){
    circle(x,x+2,1);
  }
}

void drawQuadraticFunction(){
  
 /*
 f(x) = 10x^2 + 2x -5
 */
 
 for(float x= -300;x<=300;x+=0.1){
  
   circle(x*10, (float)Math.pow(x,2) + (2*x) - 5, 1);
   
 }
}

float radius=50;

void drawCircleFunction(){
  
  for(int x=0; x<360; x++){
   
    circle((float)Math.cos(x)*radius, (float)Math.sin(x)*radius, 1);
    
  }
}

int spacing=3;
int widthOfWave = width + 16;
float theta = 0.0;
float heightOfWave = 50;
float period = 100.0;
float dx;
float[] yValue;

void drawWaveFunction(){
  
  theta += 0.03;
  
  float x =theta;
  
  for(int i=0;i<yValue.length;i++){
    yValue[i] = sin(x) * heightOfWave;
    x += dx;
  }
}

void rederStroke(){
  
  //noStroke();
  //fill(200);
   fill(138,43,226);  //RGB(decimal code) rannge : 0-255
   
   for (int x = 0; x < yValue.length; x++){
     ellipse(-550+x*spacing,yValue[x], 10, 10);
   }
}

void keyPressed(){
  
  switch(key){
    case'a':
    case'A':
    theta += 0.30;
    break;
    
    
  }
}
